Editor for Smali language used in Android apps.

To install the plugin into your eclipse workbench use the site location: https://gitlab.com/s4us/SmaliEditor/raw/master/updatesite

The project was additionally used to learn the development of eclipse editor plugins.

Feature list:

 Syntax highlighting to color language token using a fast partioner and scanner
 
 Annotation to allow folding of methods
 
 Outline view to show list of defined methods
 
 Actions on outline view to remove methods
 
 Hyperlink on classes, fields or methods to easily jump to it
 
 Hover on classes, fields or methods to show their declaration without switching the editor
 
 Double click support to mark occurrences of fields or methods inside a class
 
 Content assistant to add an empty method
 
 All editor features backed by an abstract syntax tree (AST)
 